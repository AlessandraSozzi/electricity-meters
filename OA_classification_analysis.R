clusters = read.csv("clusters.csv")
colnames(clusters) = c("oa11", "Cluster", "ClusterName")

clusters_outliers = read.csv("clusters_outliers.csv")
colnames(clusters_outliers) = c("oa11", "Cluster", "ClusterName")

area = read.csv("area.csv")
leeds = read.csv("leeds.csv")
OAC_Input <- read.csv("/Users/Alessandra/GitHub/2011OAC/Documents/Input/2011_OAC_Raw_kVariables.csv")

OAC_Input_subset = subset(OAC_Input, OA %in% area$oa11)
OAC_Input_leeds = subset(OAC_Input, OA %in% leeds$oa11)
OAC_Input_merged = merge(OAC_Input_subset, area[, c(2, 6)], by.y = "oa11", by.x = "OA", all = TRUE)
OAC_Input_merged_leeds = merge(OAC_Input_leeds, leeds[, c(2, 6)], by.y = "oa11", by.x = "OA", all = TRUE)
write.csv(OAC_Input_merged, "OAC_Input_merged.csv")
write.csv(OAC_Input_merged_leeds, "OAC_Input_merged_leeds.csv")

#OAC_Converted_Transformed_Range_Cluster_Mean_Global_Mean<- colMeans(OAC_Converted_Transformed_Range)

OAC_Converted_Transformed_Range_df = data.frame(OAC_Converted_Transformed_Range)
OAC_Converted_Transformed_Range_df$oa11 = rownames(OAC_Converted_Transformed_Range_df)

Global_Mean<- colMeans(subset(OAC_Converted_Transformed_Range_df, oa11 %in% clusters$oa11)[,-61])
# Area Category mean : Positive, Negative, Mixed, Normal
categories = levels(area$Category)
area_mixed = subset(area, Category == categories[1])
area_negative = subset(area, Category == categories[2])
area_normal = subset(area, Category == categories[3])
area_positive = subset(area, Category == categories[4])
Positive_Mean = colMeans(subset(OAC_Converted_Transformed_Range_df, oa11 %in% area_positive$oa11)[,-61])
Negative_Mean = colMeans(subset(OAC_Converted_Transformed_Range_df, oa11 %in% area_negative$oa11)[,-61])
Normal_Mean = colMeans(subset(OAC_Converted_Transformed_Range_df, oa11 %in% area_normal$oa11)[,-61])
Mixed_Mean = colMeans(subset(OAC_Converted_Transformed_Range_df, oa11 %in% area_mixed$oa11)[,-61])

df_means = rbind(Positive_Mean, Negative_Mean, Mixed_Mean, Normal_Mean)
write.csv(df_means, "df_means.csv")

OAC_Converted_Transformed_Range_With_ClusterCode = merge(OAC_Converted_Transformed_Range_df, clusters, by= "oa11", all.x = TRUE)
Mean_Out_Tab<- data.frame(names(Global_Mean))
colnames(Mean_Out_Tab)<- c("Cluster_Mean_Var")

OAC_Converted_Transformed_Range_With_ClusterCode_V2 = merge(OAC_Converted_Transformed_Range_df, clusters_outliers, by= "oa11", all.x = TRUE)
Mean_Out_Tab_V2<- data.frame(names(Global_Mean))
colnames(Mean_Out_Tab_V2)<- c("Cluster_Mean_Var")



# Deviation of supergroup mean from total postcodes mean
for (i in 1:8)
{
  Cluster_Mean_Sel<- OAC_Converted_Transformed_Range_With_ClusterCode[which(OAC_Converted_Transformed_Range_With_ClusterCode$Cluster==i),2:c(ncol(OAC_Converted_Transformed_Range_With_ClusterCode)-2)]
  
  Cluster_Mean_Mean<- data.frame(colMeans(Cluster_Mean_Sel))
  Mean_Out_Col<- Cluster_Mean_Mean-data.frame(Global_Mean)
  colnames(Mean_Out_Col)<- c(paste("Cluster_", i, sep=""))

  Mean_Out_Tab<- data.frame(Mean_Out_Tab, Mean_Out_Col)
}

# deviation of outliers mean
for (i in 1:8) {
  Cluster_Mean_Sel<- OAC_Converted_Transformed_Range_With_ClusterCode_V2[which(OAC_Converted_Transformed_Range_With_ClusterCode_V2$Cluster==i),2:c(ncol(OAC_Converted_Transformed_Range_With_ClusterCode_V2)-2)]
  
  Cluster_Mean_Mean<- data.frame(colMeans(Cluster_Mean_Sel))
  Mean_Out_Col<- Cluster_Mean_Mean-data.frame(Global_Mean)
  colnames(Mean_Out_Col)<- c(paste("Cluster_", i, sep=""))
  
  Mean_Out_Tab_V2<- data.frame(Mean_Out_Tab_V2, Mean_Out_Col)
}

Mean_Out_VarID <- data.frame (1:nrow(Mean_Out_Tab),Mean_Out_Tab)
colnames(Mean_Out_VarID) <- c("Cluster_Mean_VarID",names(Mean_Out_Tab))
# Check:  Mean_Out_VarID[27:30,]

Mean_Out_VarID_V2 <- data.frame (1:nrow(Mean_Out_Tab_V2),Mean_Out_Tab_V2)
colnames(Mean_Out_VarID_V2) <- c("Cluster_Mean_VarID",names(Mean_Out_Tab_V2))
# Check:  Mean_Out_VarID_V2[27:30,]

write.csv(Mean_Out_VarID, "cluster_deviations.csv")
write.csv(Mean_Out_VarID_V2, "cluster_outliers_deviations.csv")
