import pandas as pd
import glob
import re
import numpy as np

# Function to read multiple files from a specified directory
def read_data(path, index = 0):
    # Read all files in the directory
    filenames = glob.glob(path + "/*.csv")
    # Concatenate all data into one DataFrame
    dfs = []
    for filename in filenames:
        dfs.append(pd.read_csv(filename, index_col = index, dtype = str))

    return pd.concat(dfs)


# Import postcode electricity data - 4 files
# Number of electricity meters and consumption levels in Great Britain at postcode level
# Domestic Electricity estimates
# Year 2013

directory_path = r'/Users/Alessandra/GitHub/Electricity_meters/Data/electricity_meters'
elec = read_data(directory_path)
elec.index = [str(item).strip() for item in elec.index]

# Remove comma
colnames = list(elec.columns.values)
for n in range(len(colnames)):
    elec[colnames[n]] = [float(str(item).replace(",", "").replace("-","NaN").strip()) for item in elec[colnames[n]]]


# Import postcodes from Scotland
scotland = pd.read_csv('/Users/Alessandra/GitHub/Electricity_meters/Data/scotland_postcodes/scotland_postcodes.csv', 
                        index_col = 0, 
                        #names = ['Postcode', 'Latitude'],
                        dtype = str)
                        
scotland_districts = list(set([item.split()[0] for item in scotland.index.values]))
                        
# Remove Scotland postcodes and Scotalnd districts
elec_EW = elec[~elec.index.isin(scotland_districts)].copy() # 1,156,393
elec_EW = elec_EW[~elec_EW.index.isin(scotland.index)] # 1,046,423

# 109,970 were Scotland postcodes
# 398 were Scotland districts

# Remove districts from England and Wales
EW_districts = list(set([item.split()[0] for item in elec_EW.index]))
elec_final = elec_EW[~elec_EW.index.isin(EW_districts)].copy() # 1,044,112

# 2,311 were England and Wales districts


# Import Address Register Data - 2 files
# Year 2015 (possibly)
#

directory_path = r'/Users/Alessandra/GitHub/Electricity_meters/Data/address_register'
addr = read_data(directory_path, 6)
#addr = addr[['Single_address', 'Residential']]
addr.index.names = ['Postcode']
# addr data: 1,451,641 observations

# Merge Address register with electricity meters
meters_all = pd.merge(elec_final, addr, left_index = True, right_index=True) # Final: 1,041,672

excluded = pd.DataFrame({'pcds': list(set(set(elec_final.index) - set(meters_all.index)))}) # 2,440
excluded_addr = pd.DataFrame({'pcds': list(set(set(addr.index) - set(meters_all.index)))})


all_postcodes2015 = pd.read_csv('/Users/Alessandra/GitHub/Electricity_meters/Data/all_postcodes_2015/ONSPD_AUG_2015_UK.csv', 
                        dtype = str)
                        
all_postcodes2015 = all_postcodes2015[['pcds','dointr','doterm', 'lat', 'long', 'oa11', 'msoa11', 'lsoa11', 'oslaua']]
all_postcodes2015['pcds'] = [str(item).strip() for item in all_postcodes2015['pcds']]

## Merge elec final with all_postcodes
meters_OA_postcodes = pd.merge(elec_final, all_postcodes2015[['pcds','lat', 'long', 'oa11']], left_index = True, right_on="pcds")
meters_OA_postcodes.to_csv('/Users/Alessandra/GitHub/Electricity_meters/Data/meters_OA_postcodes.csv',index = False)

##

all_postcodes2015['year_introduction'] = [float(str(item)[0:4]) for item in all_postcodes2015['dointr']]
all_postcodes2015['year_termination'] = [float(str(item)[0:4]) for item in all_postcodes2015['doterm']]

not_matched = pd.merge(excluded, all_postcodes2015, on = 'pcds', how = 'left')

terminated = not_matched[~not_matched['doterm'].isnull()]
introduced = not_matched[not_matched['doterm'].isnull()]

terminated_byYear = terminated[['year_termination', 'doterm']].groupby('year_termination').count()
introduced_byYear = introduced[['year_introduction', 'dointr']].groupby('year_introduction').count()

# 67 + 19 + 6 = 92 terminated after 2012 -> 4.2 %
# 59 + 70 + 16 = 145 introduced from 2013 and above -> 56.4 % of 257


# Analysis of matching incongruencies between Address Register data and eletricity meters data 
addr_missing = pd.merge(excluded_addr, all_postcodes2015, on = 'pcds', how = 'left')
addr_introduced_byYear = addr_missing[['year_introduction', 'dointr']].groupby('year_introduction').count()

addr_prior2012 = addr_missing[addr_missing['year_introduction'] <= 2012].copy()
addr_prior2012 = pd.merge(addr_prior2012, addr[['Single_address', 'Residential']], right_index = True, left_on = 'pcds', how = 'left')

addr_prior2012.Residential.fillna(0, inplace=True)

addr_prior2012.to_csv('addr_prior2012.csv')



## Remove extra columns
meters_all = meters_all[['Total_consumption','Meters','Mean', 'Median', 'Single_address', 'Residential']]



# Add OA, MSOA, LSOA, LA fields
meters = pd.merge(meters_all, all_postcodes2015[['pcds', 'lat', 'long', 'oa11', 'msoa11', 'lsoa11', 'oslaua']], left_index = True, right_on = 'pcds', how = 'left')
#  1,041,672 observations

# Number of postcodes for each Output Area
addr_all = pd.merge(addr, all_postcodes2015[['pcds', 'lat', 'long', 'oa11', 'msoa11', 'lsoa11', 'oslaua']], left_index = True, right_on = 'pcds', how = 'left')
sum(addr_all['oa11'].isnull()) # 0 NaN
OA_counts = addr_all[['oa11', 'Residential']].groupby('oa11').count()
OA_counts.columns = ['n_postcodes']

#elec_EW.dropna(inplace = True)
# elec data: 1,153,920 observations


# The final dataset is composed of 1,041,672 postcodes

############################################################################################

# Number of missing values for each column
[sum(pd.isnull(meters[colname])) for colname in list(meters.columns.values)]
# Residential: 1060 NaNs

meters.Residential.fillna(0, inplace=True)

meters['difference'] = meters['Meters'].astype(float) - meters['Residential'].astype(float)

meters['Residential'] = meters['Residential'].astype(float)


diff_mean = meters['difference'].mean()
diff_std = meters['difference'].std()
meters['Outliers']  = (meters['difference'] > (diff_mean + 2.58 * diff_std)) | (meters['difference'] < (diff_mean - 2.58 * diff_std))

# +/- 2.58 std -> [-13.8, 13.53]

# Positives outliers
sum(meters['Outliers'] & (meters['difference'] > 0)) # 6,277 postcodes
meters[meters['Outliers'] & (meters['difference'] > 0)].describe()

# Negatives outliers
sum(meters['Outliers'] & (meters['difference'] < 0)) # 9,531 postcodes
meters[meters['Outliers'] & (meters['difference'] < 0)].describe()




meters.to_csv('/Users/Alessandra/GitHub/Electricity_meters/Data/metersData.csv', index = False)



# Import Postcodes clasification - 1 file
OA_File = '/Users/Alessandra/GitHub/Electricity_meters/Data/OA_classification/2011OAC.csv'
OAclass = pd.read_csv(OA_File,
                        index_col = 0,
                        dtype = str)


meters_class = pd.merge(meters, OAclass[['Supergroup Code', 'Supergroup Name']], left_on = 'oa11', right_index = True, how = 'left')

meters_class.to_csv('/Users/Alessandra/GitHub/Electricity_meters/Data/metersClass.csv', index = False)

Outliers = meters_class[meters_class['Outliers'] == True].copy()
Positives = Outliers[Outliers['difference'] > 0]
Negatives = Outliers[Outliers['difference'] < 0]
Positives_OA = Positives[['difference', 'oa11']].groupby('oa11').count()
Positives_OA.columns = ['Positives']
Negatives_OA = Negatives[['difference', 'oa11']].groupby('oa11').count()
Negatives_OA.columns = ['Negatives']


outliers_total = Outliers[['oa11', 'difference']].groupby('oa11').count()
outliers_total.columns = ['Outliers_postcodes']


outliers_rate = pd.merge(OA_counts, outliers_total, left_index = True, right_index = True, how = 'left')
outliers_rate.Outliers_postcodes.fillna(0, inplace=True)
outliers_rate = pd.merge(outliers_rate, Positives_OA, left_index = True, right_index = True, how = 'left')
outliers_rate.Positives.fillna(0, inplace=True)
outliers_rate = pd.merge(outliers_rate, Negatives_OA, left_index = True, right_index = True, how = 'left')
outliers_rate.Negatives.fillna(0, inplace=True)



outliers_rate['rate'] = outliers_rate['Outliers_postcodes'].astype(float)/outliers_rate['n_postcodes'].astype(float)


# Analisis of number of outliers per Output Area

# Number of oulier postcodes per OA / Number of postcode per OA (in the Address Register)

Outliers = meters_class[meters_class['Outliers'] == True].copy()
Outliers['type'] = Outliers['difference'] > 0
Positives = Outliers[Outliers['difference'] > 0]
Negatives = Outliers[Outliers['difference'] < 0]


Positives_OA = Positives[['difference', 'oa11']].groupby('oa11').count()
Positives_OA.columns = ['Positives']

Negatives_OA = Negatives[['difference', 'oa11']].groupby('oa11').count()
Negatives_OA.columns = ['Negatives']

Lookup = '/Users/Alessandra/GitHub/Electricity_meters/Lookup/2011 OAC May 2014 ONSPD Lookup.csv'
look = pd.read_csv(Lookup)


# Hounslow, Richmond upon Thames, Ealing
# Local Authorities: E09000018, E09000027, E09000009

focus_area = meters_class[meters_class['oslaua'].isin(['E09000018', 'E09000027', 'E09000009'])] # 11,829 total postcodes
area = focus_area[['oslaua', 'oa11','pcds']].groupby(['oslaua', 'oa11']).count()
area = area.reset_index()
area = pd.merge(area, Positives_OA, left_on = 'oa11', right_index = True, how = 'left')
area = pd.merge(area, Negatives_OA, left_on = 'oa11', right_index = True, how = 'left')
area.Positives.fillna(0, inplace=True)
area.Negatives.fillna(0, inplace=True)
def cat(item) :
    if(item['Positives'] > 0 and item['Negatives'] > 0):
        return 'Mixed'
    elif(item['Positives'] > 0 and item['Negatives'] == 0):
        return 'Positive'
    elif(item['Positives'] == 0 and item['Negatives'] > 0):
        return 'Negative'
    else: 
        return 'Normal'
        
        
area['Category'] = [cat(area.loc[x]) for x in range(len(area))]
area[['oa11', 'Category']].groupby('Category').count()

# Mixed: 15
# Negative: 314
# Normal: 1916
# Positive: 39


area.to_csv('area.csv', index = False)


byOA11 = Outliers[['oa11', 'difference']].groupby('oa11').sum()
byOA11 = pd.merge(byOA11, Positives_OA, left_index = True, right_index = True, how = 'left')
byOA11 = pd.merge(byOA11, Negatives_OA, left_index = True, right_index = True, how = 'left')
byOA11.Positives.fillna(0, inplace=True)
byOA11.Negatives.fillna(0, inplace=True)


la = pd.read_csv('Output_areas_(2011)_to_lower_layer_super_output_areas_(2011)_to_middle_layer_super_output_areas_(2011)_to_local_authority_districts_(2011)_E+W_lookup/OA11_LSOA11_MSOA11_LAD11_EW_LUv2.csv', dtype = str)
la = la[['OA11CD', 'LAD11CD', 'LAD11NM']]

byOA11 = pd.merge(byOA11, la, left_index = True, right_on = 'OA11CD', how = 'left')

byla = byOA11[['Positives','Negatives','LAD11CD']]  .groupby('LAD11CD').aggregate(np.sum, np.sum)
byla['diff'] = byla['Positives'] - byla['Negatives']
sorted(byla['diff'])[-10:]

byla[byla['diff'] > 50]

focus_leeds = meters_class[meters_class['oslaua'].isin(['E08000035'])]

leeds = focus_leeds[['oslaua', 'oa11','pcds']].groupby(['oslaua', 'oa11']).count()
leeds = leeds.reset_index()
leeds = pd.merge(leeds, Positives_OA, left_on = 'oa11', right_index = True, how = 'left')
leeds = pd.merge(leeds, Negatives_OA, left_on = 'oa11', right_index = True, how = 'left')
leeds.Positives.fillna(0, inplace=True)
leeds.Negatives.fillna(0, inplace=True)

leeds['Category'] = [cat(leeds.loc[x]) for x in leeds.index]
leeds[['oa11', 'Category']].groupby('Category').count()
leeds.to_csv('leeds.csv', index = False)



# Telford and Wrekin

tw = pd.read_csv('Data/BD_decc4.csv')
meters = pd.read_csv('/Users/Alessandra/GitHub/Electricity_meters/Data/metersData.csv')
OA_classification = pd.read_csv('/Users/Alessandra/GitHub/Electricity_meters/Data/OA_classification/2011OAC.csv')
OA_meters_avg_hs = pd.read_csv('/Users/Alessandra/GitHub/Electricity_meters/Data/OA_meters_avg_hs.csv')
OAC_Input = pd.read_csv("/Users/Alessandra/GitHub/2011OAC/Documents/Input/2011_OAC_Raw_kVariables.csv")
OAC_Input = OAC_Input[['OA','k027', 'k028', 'k029', 'k030']]
OAC_Input.columns = ['oa11', 'HH detached house', 'HH semi-detached house', 'HH terrace', 'HH flat']
meters['pcds_v2'] = [''.join(x.split(' ')) for x in meters['pcds']]
tw = pd.merge(tw, meters, how = 'left', left_on= 'POSTCODE', right_on='pcds_v2')
tw = pd.merge(tw, OAC_Input, how = 'left', on= 'oa11')

tw = tw[~tw['pcds_v2'].isnull()]

tw['Residential'] = tw['count']
tw['difference'] = tw['Residential'] - tw['Meters']
tw['Outliers'] = (tw['difference'] <= -14) | (tw['difference'] >= 14)

tw = tw[['POSTCODE', 'Residential', 'Meters', 'difference', 'Outliers', 'Total_consumption', 'Mean', 'Median', 'oa11', 'HH detached house', 'HH semi-detached house', 'HH terrace', 'HH flat']]

tw.to_csv('Data/tw.csv', index = False)
